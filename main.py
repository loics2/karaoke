from flask import Flask
from flask import render_template
from flask import request
from fuzzywuzzy import process
from random import randrange
import csv

app = Flask(__name__)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.route('/', methods=['POST', 'GET'])
def search_catalogue():
    res = None

    def processor(x):
        if isinstance(x, list):
            return " ".join([x[2], x[1]])
        else:
            return x

    if request.method == 'POST':
        search = request.form['search']
        empty = ["On n'a pas ça, espèce de hipster..."]
        if search == "":
            return render_template('index.html',
                                   res=empty)

        with open('./karafuncatalog.csv') as opened:
            reader = csv.reader(opened, delimiter=';')
            res = process.extract(search, reader,
                                  processor=processor,
                                  limit=30)

            res = [(song[0][2], song[0][1]) for song in res if song[1] >= 70]
            if res is None:
                res = empty

    return render_template('index.html', res=res)


@app.route('/random', methods=['GET'])
def random_song():
    with open('./karafuncatalog.csv') as opened:
        reader = csv.reader(opened, delimiter=';')
        res = next(reader)
        for num, line in enumerate(reader):
            if randrange(num + 2):
                continue
            res = line
        return render_template('random.html', song=(res[2], res[1]))
