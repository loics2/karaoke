FROM python:3.6.2
ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt
ENV FLASK_APP=main.py
CMD ["flask", "run", "--host=0.0.0.0"]